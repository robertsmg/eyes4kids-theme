<?php
get_header();

?>
<section class="go-back">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a href="<?=site_url()."/blog"?>" class="back-blog">
                    <h3>
                        <i class="fa fa-arrow-left"></i>
                        <?php _e("Go back to articles");?>
                    </h3>
                </a>

                <hr>
            </div>
        </div>
    </div>
</section>
<section class="posts-columns mb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-col">

                <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post();?>
                <div class="row mb-5">

                    <div class="col-sm-12">
                        <a href="<?php the_permalink() ?>" class="title">
                            <?php the_title() ?>
                        </a>

                        <div class="post-meta">
                            <?php $u_time = get_the_time('U');
                           $u_modified_time = get_the_modified_time('U');
                           if ($u_modified_time >= $u_time + 86400) {?>
                            <?php echo __("Updated")." ".get_the_modified_time('F jS, Y');
                                                                    }else{
                               the_time( 'F d, Y');
                           }
                            ?>
                            <?=__("by");?>
                            <?php the_author_posts_link();?>
                            <br>
                            <div class="share-post">
                                <span>
                                    <?=__("Share this article");?>:</span>
                                <ul class="list-social">
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=get_permalink()?>&t=<?=the_title()?>"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://plus.google.com/share?url=<?=get_permalink()?>"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="http://pinterest.com/pin/create/button/?url=<?=get_permalink()?>&description=<?=the_title()?>"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <?php if (has_post_thumbnail()) { ?>
                        <div class="img-holder">
                            <a href="<?php the_permalink() ?>">
                                <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                            </a>
                        </div>
                        <?php } ?>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </div>

                </div>
                <?php endwhile;?>
                <?php
                else :
                get_template_part('templates/content', 'none');
                endif;
                ?>
            </div>
            <div class="col-lg-4">
                <?php get_template_part("templates/blog-side"); ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-center">
                <div class="navigation">
                    <p>
                        <?php posts_nav_link('&#8734;','Previous posts','Next posts'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();
