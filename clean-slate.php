<?php
/***
Template Name: Clean Slate
 */
get_header();
the_post();
//$home = get_page_by_path("home");

if(get_current_blog_id() == "1"){
    $home = get_page_by_path("home");
}else{
    $home = get_page_by_path("inicio");
}
?>

<section class="empty-page-content">
    <div class="container">
        <div class="row">
            <section class="posts-columns mb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 posts-col">
                            <?php the_content(); ?>
                        </div>
                        <div class="col-lg-4">
                            <?php get_template_part("templates/blog-side"); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 text-center">
                            <div class="navigation">
                                <p>
                                    <?php posts_nav_link('&#8734;','Previous posts','Next posts'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<?php get_footer();
