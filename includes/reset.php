<?php
add_theme_support( 'custom-logo' );
add_theme_support( 'post-thumbnails' );
add_filter('show_admin_bar', '__return_false');
add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

if(!is_admin()){
    add_action( 'wp_enqueue_scripts', 'add_front_scripts' );
}

function add_menus() {
    register_nav_menus(
        array(
            'main_nav' => 'The main menu',
        ));
}
add_action( 'init', 'add_menus' );
//add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

if(!is_admin()){
    add_action( 'wp_enqueue_scripts', 'add_front_scripts' );
}

function add_front_scripts(){
    global $wp_query;
    wp_deregister_script('jquery');
    wp_deregister_script('bootstrap');
    wp_register_script('jquery','http://code.jquery.com/jquery-3.3.1.min.js', false, THEME_VERSION);
    wp_enqueue_script('jquery');

    wp_enqueue_style( 'bt', "//maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css",[],THEME_VERSION);
    wp_enqueue_style( 'fa', "https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css",[],THEME_VERSION);

    wp_enqueue_script( THEME_PREFIX.'theter', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array('jquery'), THEME_VERSION, true );
    wp_enqueue_script( THEME_PREFIX.'bts', '//maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js', array('jquery'), THEME_VERSION, true );
    wp_enqueue_script( THEME_PREFIX.'ss', '//cdnjs.cloudflare.com/ajax/libs/smoothscroll/1.4.4/SmoothScroll.min.js', array('jquery'), THEME_VERSION, true );
    wp_enqueue_script( THEME_PREFIX.'jqm', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js', array('jquery'), THEME_VERSION, true );

    /*- OWL CAROUSEL -*/
    wp_register_script('owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array('jquery'), THEME_VERSION, true);
    wp_enqueue_script('owl-js');

    /*- OWL -*/
    wp_register_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css', false, THEME_VERSION, 'all');
    wp_enqueue_style('owl-css');

    /*- OWL - THEME DEFAULT -*/
    wp_register_style('owltheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css', false, THEME_VERSION, 'all');
    wp_enqueue_style('owltheme-css');

    wp_enqueue_script( 'mi-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array('jquery'),false,true);
    wp_enqueue_script( 'catpcha', 'https://www.google.com/recaptcha/api.js', array('jquery'));

    wp_enqueue_style( 'styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );
    wp_enqueue_style( 'styles-slickt', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css' );
    //    wp_enqueue_script('jquery-form');

    wp_enqueue_style( 'dppp', "https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css",[],THEME_VERSION);
    wp_enqueue_script( THEME_PREFIX.'jsssqm', 'https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js', array('jquery'), THEME_VERSION, true );
    wp_enqueue_script( THEME_PREFIX.'jsssqmm', 'https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/messages/messages.es-es.js', array('jquery'), THEME_VERSION, true );

    wp_enqueue_script( THEME_PREFIX.'bootstrap-datepicker', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js', array('jquery'), THEME_VERSION, true );

    wp_enqueue_style( 'bootstrap-datepicker', "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css",[],THEME_VERSION);

    wp_enqueue_style( THEME_PREFIX.'styles', get_stylesheet_directory_uri().'/style.min.css?time='.time(),[],THEME_VERSION);
    wp_enqueue_script( THEME_PREFIX.'default', get_stylesheet_directory_uri().'/js/default.js?'.time(), array('jquery'), THEME_VERSION, true );
    wp_enqueue_script( THEME_PREFIX.'form-controller', get_stylesheet_directory_uri().'/js/form-controller.js?'.time(), array('jquery', THEME_PREFIX.'default'), THEME_VERSION, true );
    wp_enqueue_script('jquery-form');

    wp_localize_script( THEME_PREFIX.'default', 'frontend_ajax_object',
        array(
            'admin_url' => admin_url( 'admin-ajax.php' ),
            'multiste_id' => get_current_blog_id()
        )
    );

    wp_localize_script( THEME_PREFIX.'form-controller', 'frontend_ajax_object',
        array(
            'admin_url' => admin_url( 'admin-ajax.php' )
        )
    );
}


function remove_head_scripts() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);
    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}


add_action( 'after_setup_theme', 'my_theme_setup' );
function my_theme_setup(){
    load_theme_textdomain( 'e4k', get_template_directory() . '/languages' );
}
