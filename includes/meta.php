<?php
add_action( 'cmb2_admin_init', function() {

    if (is_admin()) {
        $prefix = 'site_';

        $home_id = (int)get_option('page_on_front');

        if (!$home_id) return false;



        $cmb = new_cmb2_box(array(
            'id' => 'links_section',
            'title' => __('Links section', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' => array('id' => $home_id)
        ));

        $group_field_id = $cmb->add_field( array(
            'id'          => $prefix .'section_links',
            'type'        => 'group',
            'description' => __( '', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Link {#}', 'cmb2' ),
                'add_button'    => __( 'Add link', 'cmb2' ),
                'remove_button' => __( 'Remove link', 'cmb2' ),
                'sortable'      => false, // beta
            ),
        ) );


        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Image',
            'description' => '',
            'id'   => $prefix .'link_image',
            'type' => 'file',
            'options' => array(
                'url' => false,
            ),
        ) );
        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Text',
            'description' => '',
            'id'   => $prefix .'link_text',
            'type' => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
            ),
        ) );






        $cmb = new_cmb2_box(array(
            'id' => 'cards_section',
            'title' => __('Kids cards section', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' => array('id' => $home_id)
        ));

        $group_field_id = $cmb->add_field( array(
            'id'          => $prefix .'section_cards',
            'type'        => 'group',
            'description' => __( '', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Card {#}', 'cmb2' ),
                'add_button'    => __( 'Add Card', 'cmb2' ),
                'remove_button' => __( 'Remove Card', 'cmb2' ),
                'sortable'      => false, // beta
            ),
        ) );


        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Image',
            'description' => '',
            'id'   => $prefix .'card_image',
            'type' => 'file',
            'options' => array(
                'url' => false,
            ),
        ) );
        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Text',
            'description' => '',
            'id'   => $prefix .'card_text',
            'type' => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
            ),
        ) );


        /* DESCANSO */
        $cmb = new_cmb2_box(array(
            'id' => 'home_appointment_section',
            'title' => __('Appointment section', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' => array('id' => $home_id)
        ));

        $group_field_id = $cmb->add_field( array(
            'id'          => $prefix .'appointment_links',
            'type'        => 'group',
            'description' => __( '', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Link {#}', 'cmb2' ),
                'add_button'    => __( 'Add link', 'cmb2' ),
                'remove_button' => __( 'Remove link', 'cmb2' ),
                'sortable'      => false, // beta
            ),
        ) );


        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Text',
            'description' => '',
            'id'   => $prefix .'link_text',
            'type' => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Button Text',
            'description' => '',
            'id'   => $prefix .'link_button',
            'type' => 'text',
            'options' => array(
                'url' => false,
            ),
        ) );



        $cmb = new_cmb2_box(array(
            'id' => 'map_section',
            'title' => __('Map section', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' => array('id' => $home_id)
        ));


        $cmb->add_field( array(
            'name'    => 'Address',
            'desc'    => '',
            'id'      => $prefix.'address_text',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 12,
            ),
        ) );




        //$forms_page = get_page_by_path("patient-forms");

        $cmb = new_cmb2_box(array(
            'id' => 'complete_packet_section',
            'title' => __('Complete forms packet', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2,
            'show_on' =>[ 'key' => 'page-template', 'value' => 'patient-forms.php' ]
        ));
        $cmb->add_field( array(
            'name'    => 'File Title',
            'desc'    => '',
            'id'      => $prefix.'complete_packet_text',
            'type'    => 'text',
        ) );
        $cmb->add_field( array(
            'name'    => 'Pdf File',
            'desc'    => '',
            'id'      => $prefix.'address_text',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
        ) );
        $cmb = new_cmb2_box(array(
            'id' => 'individual_packet_section',
            'title' => __('Individual forms', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' =>[ 'key' => 'page-template', 'value' => 'patient-forms.php' ]
        ));
        $group_field_id = $cmb->add_field( array(
            'id'          => $prefix .'individual_packets',
            'type'        => 'group',
            'description' => __( '', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'File {#}', 'cmb2' ),
                'add_button'    => __( 'Add file', 'cmb2' ),
                'remove_button' => __( 'Remove file', 'cmb2' ),
                'sortable'      => false, // beta
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Text',
            'description' => '',
            'id'   => $prefix .'packet_text',
            'type' => 'text',

        ) );
        $cmb->add_group_field( $group_field_id, array(
            'name' => 'File',
            'description' => '',
            'id'   => $prefix .'packet_file',
            'type' => 'file',
            'options' => array(
                'url' => false,
            ),
        ) );


        $cmb = new_cmb2_box(array(
            'id' => 'extra_section_complete_packet_section',
            'title' => __('Extra info section', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' =>[ 'key' => 'page-template', 'value' => 'patient-forms.php' ]
        ));
        $cmb->add_field( array(
            'name'    => 'Extra info',
            'desc'    => '',
            'id'      => $prefix.'extra_packet_info',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 12,
            ),
        ) );





        //$forms_page = get_page_by_path("resources");

        $cmb = new_cmb2_box(array(
            'id' => 'resources_section',
            'title' => __('Resources', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' =>[ 'key' => 'page-template', 'value' => 'resources.php' ]
        ));

        $group_field_id = $cmb->add_field( array(
            'id'          => $prefix .'resources_list',
            'type'        => 'group',
            'description' => __( '', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Resource {#}', 'cmb2' ),
                'add_button'    => __( 'Add Resource', 'cmb2' ),
                'remove_button' => __( 'Remove Resource', 'cmb2' )
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Title',
            'description' => '',
            'id'   => $prefix .'Resource_title',
            'type' => 'textarea_small',

        ) );

        $cmb->add_group_field( $group_field_id,array(
            'name'    => 'Resource text',
            'desc'    => '',
            'id'      => $prefix.'resource_text',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 12,
            ),
        ) );





        //$about_page = get_page_by_path("about");

        $cmb = new_cmb2_box(array(
            'id' => 'about_page',
            'title' => __('As seen on section', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'rows_limit'   => 2, // custom attribute to use in our JS
            'show_on' =>[ 'key' => 'page-template', 'value' => 'about.php' ]
        ));

        $group_field_id = $cmb->add_field( array(
            'id'          => $prefix .'about_icon_list',
            'type'        => 'group',
            'description' => __( '', 'cmb2' ),
            'options'     => array(
                'group_title'   => __( 'Icon {#}', 'cmb2' ),
                'add_button'    => __( 'Add Icon', 'cmb2' ),
                'remove_button' => __( 'Remove Icon', 'cmb2' )
            ),
        ) );

        $cmb->add_group_field( $group_field_id, array(
            'name' => 'Icon',
            'description' => '',
            'id'   => $prefix .'icon_img',
            'type' => 'file',
            'options' => array(
                'url' => false,
            ),

        ) );
        $cmb = new_cmb2_box(array(
            'id' => 'about_page_bottom',
            'title' => __('Bottom Banner', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'show_on' =>[ 'key' => 'page-template', 'value' => 'about.php' ]
        ));
        $cmb->add_field(array(
            'name' => 'Bottom banner text',
            'description' => '',
            'id'   => $prefix .'bottom_text',
            'type' => 'wysiwyg'
        ) );


        $cmb = new_cmb2_box(array(
            'id' => 'shopg_page_bottom',
            'title' => __('Bottom Banner', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'show_on' =>[ 'key' => 'page-template', 'value' => 'shop-glasses.php' ]
        ));
        $cmb->add_field(array(
            'name' => 'Bottom banner text',
            'description' => '',
            'id'   => $prefix .'bottom_text_sg',
            'type' => 'wysiwyg'
        ) );

        $cmb = new_cmb2_box(array(
            'id' => 'shopg_page_subtitle',
            'title' => __('Banner subtitle', 'cmb2'),
            'object_types' => array('page'), // Post type
            'context' => 'normal',
            'priority' => 'high',
            'show_names' => true,
            'show_on' =>[ 'key' => 'page-template', 'value' => 'shop-glasses.php' ]
        ));
        $cmb->add_field(array(
            'name' => 'Top banner subtitle',
            'description' => '',
            'id'   => $prefix .'top_text_sg',
            'type' => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 6,
            ),
        ) );

    }//admin

},9999);
