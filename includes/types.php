<?php


/*function custom_types()
{
    $labels = array(
        'name' => _x('Lecciones', 'Post Type General Name', 'mvv'),
        'singular_name' => _x('Lección', 'Post Type Singular Name', 'mvv'),
        'menu_name' => __('Lecciones', 'mvv'),
        'name_admin_bar' => __('Lecciones', 'mvv'),
        'archives' => __('Item Archives', 'mvv'),
        'attributes' => __('Item Attributes', 'mvv'),
        'parent_item_colon' => __('Parent Item:', 'mvv'),
        'all_items' => __('All Items', 'mvv'),
        'add_new_item' => __('Add New Item', 'mvv'),
        'add_new' => __('Add New', 'mvv'),
        'new_item' => __('New Item', 'mvv'),
        'edit_item' => __('Edit Item', 'mvv'),
        'update_item' => __('Update Item', 'mvv'),
        'view_item' => __('View Item', 'mvv'),
        'view_items' => __('View Items', 'mvv'),
        'search_items' => __('Search Item', 'mvv'),
        'not_found' => __('Not found', 'mvv'),
        'not_found_in_trash' => __('Not found in Trash', 'mvv'),
        'featured_image' => __('Featured Image', 'mvv'),
        'set_featured_image' => __('Set featured image', 'mvv'),
        'remove_featured_image' => __('Remove featured image', 'mvv'),
        'use_featured_image' => __('Use as featured image', 'mvv'),
        'insert_into_item' => __('Insert into item', 'mvv'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'mvv'),
        'items_list' => __('Items list', 'mvv'),
        'items_list_navigation' => __('Items list navigation', 'mvv'),
        'filter_items_list' => __('Filter items list', 'mvv'),
    );
    $args = array(
        'label' => __('Lección', 'mvv'),
        'description' => __('Lecciones post type', 'mvv'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail','page-attributes'),
       // 'taxonomies'            => array( 'lecciones_cat'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => false,
        'capability_type' => 'page',
    );
    register_post_type('lecciones', $args);


    $labels = array(
        'name' => _x('Categories', 'taxonomy general name'),
        'singular_name' => _x('Category', 'taxonomy singular name'),
        'search_items' => __('Search Categories'),
        'popular_items' => __('Popular Categories'),
        'all_items' => __('All Categories'),
        'parent_item' => __('Parent Categories'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item' => __('Edit Category'),
        'update_item' => __('Update Category'),
        'add_new_item' => __('Add New Category'),
        'new_item_name' => __('New Category Name'),
    );
 /*   register_taxonomy('lecciones_cat', array('lecciones'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'lecciones_cat'),
    ));*/
//}

//add_action( 'init', 'custom_types', 0 );