<?php
get_header();
the_post();
global $post;
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <iframe src="https://trustspot.io/merchant/TrustModule/blocks/SMG" width="100%" height="800px" seamless allowTransparency="true" scrolling="yes" frameborder="0" style='border:none; overflow: auto;'></iframe>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php the_content() ?>
            </div>
        </div>
    </div>
</section>




<?php  get_footer();
