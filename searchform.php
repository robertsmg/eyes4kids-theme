<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <div class="input-group mb-3">
        <input type="text" id="<?php echo $unique_id; ?>" class="form-control" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', TEXT_DOMAIN ); ?>" value="<?php echo get_search_query(); ?>" name="s" />

        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</form>