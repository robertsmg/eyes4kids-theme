<?php
get_header();
global $post;
the_post();
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>

<section class="home-banner">
    <div class="container text-cont">
        <div class="row">
            <div class="col-lg-6 d-md-flex align-items-center">
                <div>
                    <?php the_content() ?>
                    <div>
                        <a href="#appointment" class="btn btn-default blue-btn">
                            <?=__("Make an appointment",TEXT_DOMAIN);?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-placeholder" style="background-image: url('<?=$image[0]?>')">

    </div>
</section>

<?php get_template_part("templates/links");?>


<?php /* get_template_part("templates/home-old-form-appointment"); */?>

<?php get_template_part("templates/home-new-form-appointment"); ?>

<?php
$meta_cards = get_post_meta($post->ID,"site_section_cards",true);
?>
<section class="kids-cards">
    <div class="container pb-5">
        <?php if (!empty($meta_cards)) { ?>
        <?php foreach ($meta_cards as $key => $card) {?>
        <div class="row py-4">
            <div class="col-md-7 d-md-flex align-items-center <?php echo $key%2==1?" order-md-12":"" ?> text-center text-md-left">
                <div class="py-4">
                    <?=apply_filters("the_content",$card["site_card_text"])?>
                </div>
            </div>
            <div class="col-md-5 d-md-flex align-items-center <?php echo $key%2==1?"":"" ?>">
                <img class="img-fluid rounded" src="<?=$card[" site_card_image"]?>" alt="">
            </div>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
</section>


<section class="map ">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-7">
                <div id="map"></div>
            </div>
            <div class="col-lg-5 col-dir  d-md-flex align-items-center">
                <div>
                    <?=get_post_meta($post->ID,"site_address_text",true)?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="reviews">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h2>
                    <?=__("What Our Patients say",TEXT_DOMAIN);?>
                </h2>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-12">
                <iframe id="reviews" src="https://trustspot.io/merchant/TrustModule/blocks/SMG" width="100%" height="600px" seamless allowTransparency="true" scrolling="auto" frameborder="0" style='border:none; overflow: auto;'></iframe>
            </div>
        </div>
    </div>
</section>
<?php
$instagrams = getLastInstagram(); //echo "<pre>",print_r($instagrams),"</pre>";
$max = 0;
$social = get_option("smg_theme_options");
?>
<section class="instagram">
    <div class="yellow-bomb d-flex justify-content-center">
        <p>
            <?=__("Follow us to get the best tips for taking care of your children’s eye health",TEXT_DOMAIN);?>
            <br>
            <a target="_blank" href="<?=$social["social-facebook"]?>"><i class="fa fa-facebook"></i></a>
            <a target="_blank" href="<?=$social["social-instagram"]?>"><i class="fa fa-instagram"></i></a>
        </p>

    </div>
    <div class="griddie">
        <?php echo do_shortcode('[instagram-feed]'); ?>
        <?php /*  foreach ($instagrams["data"] as $ins) {?>
        <span class="d-lg-block <?php echo ++$max>4?" d-none":""; ?>">
            <figure>
                <img src="<?php echo $ins['images']['low_resolution']['url'];?>" alt="">
            </figure>
        </span>
        <?php } */ ?>
    </div>
</section>



<?php
get_footer();
