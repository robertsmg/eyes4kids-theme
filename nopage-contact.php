<?php
get_header();
global $post;

$home = get_page_by_path("home");

?>

<section class="contact-page-content">

    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <form enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="appointment_form">
                    <div class="form-row mb-3">
                        <div class="col-lg-6 col-sm-6">
                            <label><?=__("Full name",TEXT_DOMAIN);?></label>
                            <input id="fullname-contact" type="text" class="form-control" required name="fullname" placeholder="<?=__("Patient's full name",TEXT_DOMAIN);?>">
                            <small class="muted d-none text-danger"><?=__("You must enter a valid insurance name",TEXT_DOMAIN);?></small>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label><?=__("Phone number",TEXT_DOMAIN);?></label>
                            <input type="text" class="form-control phone_us" required name="phone" placeholder="<?=__("Contact phone number",TEXT_DOMAIN);?>">
                            <small class="muted d-none text-danger"><?=__("You must enter a valid insurance name",TEXT_DOMAIN);?></small>
                        </div>
                    </div>
                    <div class="form-row  mb-3">
                        <div class="col-lg-6 col-sm-6">
                            <label><?=__("Email",TEXT_DOMAIN);?></label>
                            <input type="text" class="form-control" name="email" required placeholder="<?=__("Email address",TEXT_DOMAIN);?>">
                            <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                        </div>
                        <div class="col-lg-6 col-sm-6 ">
                            <label><?=__("Addressed to",TEXT_DOMAIN);?></label>
                            <div class="select-arrow">
                                <select class="form-control" name="reason">
                                    <option value=""><?=__("-Department",TEXT_DOMAIN);?></option>
                                    <option value="Dr. Warman MD"><?=__("Dr. Warman MD",TEXT_DOMAIN);?></option>
                                    <option value="Contact Lenses"><?=__("Contact Lenses",TEXT_DOMAIN);?></option>
                                    <option value="Optical Shop Surgeries"><?=__("Optical Shop",TEXT_DOMAIN);?></option>
                                    <option value="Surgeries"><?=__("Surgeries",TEXT_DOMAIN);?></option>
                                </select>
                                <small class="muted d-none text-danger"><?=__("Must select one option",TEXT_DOMAIN);?></small>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-lg-6  col-sm-6">
                            <!--                            <div class="form-row mt-0">-->
                            <!--                                <div class="col-12">-->
                            <!--                                    <label>--><?//=__("Appointment date",TEXT_DOMAIN);?><!--</label>-->
                            <!--                                    <input type="text" class="form-control  date" required name="appo_date" placeholder="--><?//=__("DD/MM/YYYY",TEXT_DOMAIN);?><!--">-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <div class="form-row mt-1">
                                <div class="col-12">
                                    <label><?=__("Booked with us before?",TEXT_DOMAIN);?></label> <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="booked_before" id="inlineRadio1" value="yes">
                                        <label class="form-check-label" for="inlineRadio1"><?=__("Yes",TEXT_DOMAIN);?></label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="booked_before" id="inlineRadio2" value="no" checked />
                                        <label class="form-check-label" for="inlineRadio2"><?=__("No",TEXT_DOMAIN);?></label>
                                    </div>
                                    <small class="muted d-none text-danger"><?=__("Must select one option",TEXT_DOMAIN);?></small>
                                    <!-- CAPTCHA FOR FINAL SERVER -->
                                    <!-- <div class="g-recaptcha mt-4" data-sitekey="6Le190EUAAAAAKkBavGtK7dH5q8D2WlUJu-gV8Ca" data-callback="onSubmit" data-size="small" style="max-width:100%; padding-bottom: 20px;transform: scale(0.8)"></div>-->
                                    <!-- CAPTCHA FOR TEST SERVER -->
                                    <div class="g-recaptcha mt-4" data-sitekey="6LcxZnMUAAAAAO0eeRNL72SWKwKBf26UjKCu-3Wb" data-callback="onSubmit" data-size="small" style="max-width:100%; padding-bottom: 20px;transform: scale(0.8)"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6  col-sm-6">
                            <label><?=__("Message",TEXT_DOMAIN);?></label>
                            <textarea class="form-control" name="message" required placeholder="<?=__("Your message",TEXT_DOMAIN);?>"></textarea>
                        </div>
                    </div>
                    <div class="form-row pt-3">
                        <div class="col-lg-12">
                            <button class="btn blue-btn btn-block"><?=__("Confirm book");?></button>
                        </div>
                    </div>
                </form>
                <?php /*
                <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="contact_form">
                    <div class="form-group">
                        <label><?=__("First Name",TEXT_DOMAIN);?>:</label>
                        <input type="text" name="name" class="form-control" required placeholder="<?=__("Your name",TEXT_DOMAIN)?>">
                    </div>
                    <div class="form-group">
                        <label><?=__("Last Name",TEXT_DOMAIN);?>:</label>
                        <input type="text" name="lastname" class="form-control" required placeholder="<?=__("Your last name",TEXT_DOMAIN)?>">
                    </div>
                    <div class="form-group">
                        <label><?=__("Email",TEXT_DOMAIN);?>:</label>
                        <input type="text" name="email" class="form-control" required placeholder="<?=__("Your email address",TEXT_DOMAIN)?>">
                    </div>
                    <div class="form-group">
                        <label><?=__("Phone Number",TEXT_DOMAIN);?>:</label>
                        <input type="text" name="phone" class="form-control" required placeholder="<?=__("Please include area code",TEXT_DOMAIN)?>">
                    </div>
                    <div class="form-group">
                        <label>Subject</label>
                        <div class="select-arrow">
                            <select class="form-control" name="subject">
                                <option>-<?=__("Select",TEXT_DOMAIN);?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?=__("Message",TEXT_DOMAIN);?>:</label>
                        <textarea class="form-control" name="message" required placeholder="<?=__("Please include area code",TEXT_DOMAIN)?>"></textarea>
                    </div>
                    <div class="text-center text-md-left">
                        <div class="g-recaptcha" data-sitekey="6Le190EUAAAAAKkBavGtK7dH5q8D2WlUJu-gV8Ca" style="max-width:100%; padding-bottom: 20px"></div>
                    </div>
                    <div class="text-center text-md-left">
                        <button class="btn blue-btn"><?php _e("Submit",TEXT_DOMAIN);?></button>
                    </div>
                */?>
            </div>
            <div class="col-lg-6 col-dir"><?=get_post_meta($home->ID,"site_address_text",true)?></div>
        </div>
    </div>

</section>



<?php get_footer();
