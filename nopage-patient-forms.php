<?php
get_header();
the_post();
global $post;
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>

<section class="banner-page"  style="background-image: url('<?=$image[0]?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-6 d-md-flex align-items-center ">
                <div>
                    <?php the_title("<h1>","</h1>"); ?>
                    <?php the_excerpt(); ?>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="patient-forms-content">
    <div class="container">
        <div class="row pb-2">
            <div class="col-12 text-center text-md-left">
                <h2><?=__("Complete forms Packet",TEXT_DOMAIN);?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center text-md-left">
                <a target="_blank" href="<?=get_post_meta($post->ID,"site_address_text",true)?>"><?=get_post_meta($post->ID,"site_complete_packet_text",true)?></a>
            </div>
        </div>
        <div class="row pb-2 mt-5">
            <div class="col-12 text-center text-md-left">
                <h2><?=__("Individual forms",TEXT_DOMAIN);?></h2>
            </div>
        </div>
        <?php foreach (get_post_meta($post->ID,"site_individual_packets",true) as $pack) {?>
        <div class="row">
            <div class="col-12 text-center text-md-left">
                <a target="_blank" href="<?=$pack["site_packet_file"]?>"><?=$pack["site_packet_text"]?></a>
            </div>
        </div>
        <?php }?>
    </div>
</section>

<section class="banner-page extra-packet-info"  style="background-image: url('<?=get_stylesheet_directory_uri()."/img/patient-forms.jpg"?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-7 d-md-flex align-items-center ">
                <div>
                   <?=apply_filters("the_content",get_post_meta($post->ID,"site_extra_packet_info",true))?>
                    <a href="<?=site_url()?>/resources" class="btn blue-btn px-5 mt-4   "><?=__("Resources",TEXT_DOMAIN);?></a>
                </div>

            </div>
        </div>
    </div>
</section>



<?php  get_footer();
