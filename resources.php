<?php
/**
 * Template Name: Resources
 *
 */
get_header();
the_post();
global $post;
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>

<section class="banner-page"  style="background-image: url('<?=$image[0]?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-5 d-md-flex align-items-center ">
                <div>
                    <?php the_title("<h1>","</h1>"); ?>
                    <?php the_content(); ?>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="resources-page-content">

    <div class="container">
        <div class="row">
            <?php foreach (get_post_meta($post->ID,"site_resources_list",true) as $resource) {?>
                <div class="col-md-6 my-2">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><?=$resource["site_Resource_title"]?></h5>
                            <div class="card-text"><?=apply_filters("the_content",$resource["site_resource_text"])?></div>
                        </div>
                    </div>
                </div>
            <?php }?>

        </div>
    </div>

</section>




<?php  get_footer();
