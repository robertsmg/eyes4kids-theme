<?php
global $post;

global $wp_query;

$name = $post->post_name;


if ( isset( $wp_query ) && (bool) $wp_query->is_posts_page ) {
    $name = "blog";
}
$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
$menu = wp_get_nav_menu_items("main-menu");

$home_id = (int)get_option('page_on_front');

$link_apo = $post->ID == $home_id? "#appointment":site_url()."#appointment";
//var_dump(get_site_url());die();

?>
<div class="fixed-top">
    <div class="prenav d-none d-lg-block d-xl-block">
        <div class="container">
            <div class="row">
                <div class="col-md-3 d-md-flex align-items-center lang-links text-center text-md-left">
                    <a href="<?=network_site_url();?>" class="<?=get_current_blog_id()=="1"?"active":""?>"><?=__("English",TEXT_DOMAIN);?></a> /
                    <a href="<?=network_site_url();?>es" class="<?=get_current_blog_id()!="1"?"active":""?>"><?=__("Spanish",TEXT_DOMAIN);?></a>
                </div>
                <div class="col-md-6 col-sub-title align-items-center d-flex">
                    <h2 class="d-none d-md-block"><?=__("Pediatric Ophthalmology Consultants",TEXT_DOMAIN);?></h2>
                </div>
                <div class="col-md-3 d-md-flex">
                    <a href="#" class="request-appointment" data-toggle="modal" data-target="#exampleModal">
                        <?=__("Request an appointment",TEXT_DOMAIN);?>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <?php

    $menu_name = 'main-menu';
    $menu = wp_get_nav_menu_object($menu_name);

    $menu_items = wp_get_nav_menu_items($menu->term_id);
    // var_dump($menu_items);
    $menu_list = '<ul class="navbar-nav ml-auto">';
    $count = 0;
    $submenu = false;
    $cpi=get_the_id();
    foreach( $menu_items as $current ) {
        if($cpi == $current->object_id ){if ( !$current->menu_item_parent ) {$cpi=$current->ID;}else{$cpi=$current->menu_item_parent;}$cai=$current->ID;break;}
    }
    if (!isset($cai)) {
        $cai = '';
    }
    foreach( $menu_items as $menu_item ) {
        $link = $menu_item->url;
        $title = $menu_item->title;
        $menu_item->ID==$cai ? $ac2=' current_menu' : $ac2='';
        if ( !$menu_item->menu_item_parent ) {
            $parent_id = $menu_item->ID;

            if(!empty($menu_items[$count + 1]) && $menu_items[ $count + 1 ]->menu_item_parent == $parent_id ){//Checking has child
                $menu_list .= '<li class="dropdown nav-item d-lg-flex align-items-center">
                <a href="'.$link.'" class="nav-link dropdown-toggle"  data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    '.$title.'
                </a>';
            }else{
                $p = "/".str_replace("/","\/",get_site_url())."/";
                $result = trim(preg_replace($p,"",$link),"/");
                $class ="";
                if ($result==$name) {
                    $class = "active";
                }
                $menu_list .= '<li class=" nav-item d-lg-flex align-items-center">' ."\n";$menu_list .=
                    '<a href="'.$link.'" class=" nav-link '.$class.'">'.$title.'</a>' ."\n";
            }

        }
        if ( $parent_id == $menu_item->menu_item_parent ) {
            if ( !$submenu ) {
                $submenu = true;
                $menu_list .= '<ul class="dropdown-menu  menu-right">' ."\n";
            }
            $menu_list .= '<li class="nav-item d-lg-flex align-items-center">' ."\n";
            $menu_list .= '<a href="'.$link.'" class="dropdown-item nav-link">'.$title.'</a>' ."\n";
            $menu_list .= '</li>' ."\n";
            if(empty($menu_items[$count + 1]) || $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu){
                $menu_list .= '</ul>';
                $submenu = false;
            }
        }
        if (empty($menu_items[$count + 1]) || $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) {
            $menu_list .= '</li>';
            $submenu = false;
        }
        $count++;
    }
    ?>

    <nav class="navbar navbar-expand-lg navbar-light main-navbar">
        <div class="container">
            <a class="navbar-brand" href="<?=site_url()?>">
                <img src="<?=$logo[0]?>" alt="<?=get_bloginfo("name")?>">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <?=$menu_list;?>

                <ul class="navbar-nav d-block d-xl-none d-lg-none">
                    <li class="nav-item">
                        <a class="nav-link" href="<?=network_site_url();?>" class="<?=get_current_blog_id()=="1"?"active":""?>"><?=__("English",TEXT_DOMAIN);?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=network_site_url();?>es" class="<?=get_current_blog_id()!="1"?"active":""?>"><?=__("Spanish",TEXT_DOMAIN);?></a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="prenav prenav-mobile d-block d-lg-none d-xl-none">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="#" class="request-appointment" data-toggle="modal" data-target="#exampleModal">
                        <?=__("Request an appointment",TEXT_DOMAIN);?>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <?php if(isset($_SESSION["message_cv"])){?>
    <div class="alert alert-success alert-e4k" role="alert">
        <div class="container">
            <?=__("Thank you! Your submission has been received. You will be contact soon to confirm your appointment.",TEXT_DOMAIN);?>
        </div>
    </div>
    <?php
                                             unset($_SESSION["message_cv"]);
                                            } ?>
</div>
