<section class="appointment d-flex" id="appointment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-img" style="background-image: url(<?=get_stylesheet_directory_uri()."/img/img-appointment.jpg" ?>)">

            </div>
            <div class="col-lg-7 d-md-flex align-items-center">
                <div class="form-holder">
                    <h2 class="mb-4"><?=__("Book an appointment with us",TEXT_DOMAIN);?></h2>
                    <?php /*
                       <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" enctype="multipart/form-data" method="post">
                           <input type="hidden" name="action" value="appointment_form">
                           <div class="form-row mb-3">
                               <div class="col-lg-5 col-sm-6">
                                   <label><?=__("Full name",TEXT_DOMAIN);?></label>
                                   <input type="text" class="form-control" required name="fullname" placeholder="<?=__("Patient's full name",TEXT_DOMAIN);?>">
                               </div>
                               <div class="col-lg-5 col-sm-6">
                                   <label><?=__("Phone number",TEXT_DOMAIN);?></label>
                                   <input type="text" class="form-control phone_us" required name="phone" placeholder="<?=__("Contact phone number",TEXT_DOMAIN);?>">
                               </div>
                           </div>
                           <div class="form-row  mb-3">
                               <div class="col-lg-5 col-sm-6">
                                   <label><?=__("Email",TEXT_DOMAIN);?></label>
                                   <input type="text" class="form-control" name="email" required placeholder="<?=__("Email address",TEXT_DOMAIN);?>">
                               </div>
                               <div class="col-lg-5 col-sm-6 ">
                                   <label><?=__("Addressed to",TEXT_DOMAIN);?></label>
                                   <div class="select-arrow">
                                       <select class="form-control" name="reason">
                                           <option value=""><?=__("-Department",TEXT_DOMAIN);?></option>
                                           <option value="Dr. Warman MD"><?=__("Dr. Warman MD",TEXT_DOMAIN);?></option>
                                           <option value="Contact Lenses"><?=__("Contact Lenses",TEXT_DOMAIN);?></option>
                                           <option value="Optical Shop Surgeries"><?=__("Optical Shop",TEXT_DOMAIN);?></option>
                                           <option value="Surgeries"><?=__("Surgeries",TEXT_DOMAIN);?></option>
                                       </select>
                                   </div>
                               </div>
                           </div>
                           <div class="form-row">
                               <div class="col-lg-5  col-sm-6">
                                   <div class="form-row mt-0">
                                       <div class="col-12">
                                           <label><?=__("Appointment date",TEXT_DOMAIN);?></label>
                                           <input type="text" class="form-control  date" required name="appo_date" placeholder="<?=__("DD/MM/YYYY",TEXT_DOMAIN);?>">
                                       </div>
                                   </div>
                                   <div class="form-row mt-3">
                                       <div class="col-12">
                                           <label><?=__("Booked with us before?",TEXT_DOMAIN);?></label> <br>
                                           <div class="form-check form-check-inline">
                                               <input class="form-check-input" type="radio" name="booked_before" id="inlineRadio1" value="yes">
                                               <label class="form-check-label" for="inlineRadio1"><?=__("Yes",TEXT_DOMAIN);?></label>
                                           </div>
                                           <div class="form-check form-check-inline">
                                               <input class="form-check-input" type="radio" name="booked_before" id="inlineRadio2" value="no" checked />
                                               <label class="form-check-label" for="inlineRadio2"><?=__("No",TEXT_DOMAIN);?></label>
                                           </div>
                                           <div class="g-recaptcha" data-sitekey="6Le190EUAAAAAKkBavGtK7dH5q8D2WlUJu-gV8Ca" style="max-width:100%; padding-bottom: 20px"></div>
                                       </div>
                                   </div>

                               </div>
                               <div class="col-lg-5  col-sm-6">
                                   <label><?=__("Message",TEXT_DOMAIN);?></label>
                                   <textarea class="form-control" name="message" required placeholder="<?=__("Your message",TEXT_DOMAIN);?>"></textarea>
                               </div>
                           </div>
                           <div class="form-row pt-5">
                               <div class="col-lg-10">
                                   <button class="btn blue-btn btn-block"><?=__("Confirm book");?></button>
                               </div>
                           </div>
                       </form>
 */?>
                    <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" id="app-form" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="contact_form">
                        <div class="form-2steps">
                            <div class="dafirst"> <!--first-step-->
                                <div class="form-row mb-3">
                                    <div class="col-lg-5 col-sm-6">
                                        <label><?=__("Patient's Name",TEXT_DOMAIN);?>:</label>
                                        <input type="text" name="name" class="form-control" required placeholder="<?=__("Your name",TEXT_DOMAIN)?>">
                                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                    <div class="col-lg-5 col-sm-6">
                                        <label><?=__("Patient's last name",TEXT_DOMAIN);?>:</label>
                                        <input type="text" name="lastname" class="form-control" required placeholder="<?=__("Your last name",TEXT_DOMAIN)?>">
                                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                </div>
                                <div class="form-row  mb-3">
                                    <!-- <div class="col-lg-5 col-sm-6"> -->
                                    <div class="col-lg-10 col-sm-12">
                                        <label><?=__("Gender",TEXT_DOMAIN);?></label>
                                        <div class="select-arrow">
                                            <select class="form-control" name="gender">
                                                <option value="Male"><?=__("Male",TEXT_DOMAIN);?></option>
                                                <option value="Female"><?=__("Female",TEXT_DOMAIN);?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-5 col-sm-6 ">
<label><?=__("Date of Birth",TEXT_DOMAIN);?></label>
<input type="text" id="datepicker-e4k" readonly name="birthday" placeholder="<?=__("Patient's birthday",TEXT_DOMAIN)?>" class="form-control">
</div> -->
                                </div>

                                <div class="form-row  mb-3">
                                    <div class="col-lg-10  col-sm-12">
                                        <label><?=__("Address",TEXT_DOMAIN);?></label>
                                        <textarea class="form-control" style="height: 70px" name="address" required placeholder="<?=__("Address",TEXT_DOMAIN);?>"></textarea>
                                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-5  col-sm-6">
                                        <label><?=__("State",TEXT_DOMAIN);?></label>
                                        <div class="select-arrow">
                                            <select class="form-control" name="state">
                                                <option value=""><?=__("-Select",TEXT_DOMAIN);?></option>
                                                <?php foreach ($us_states as $key => $state) {?>
                                                <option value="<?=$key?>"><?=$state?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5  col-sm-6">
                                        <label><?=__("Zip Code",TEXT_DOMAIN);?></label>
                                        <input type="text" class="form-control" name="zipcode" placeholder="<?=__("Zip Code Number",TEXT_DOMAIN);?>">
                                    </div>
                                </div>
                                <div class="form-row  mb-3">
                                    <div class="col-lg-10  col-sm-12">
                                        <button class="btn-sig btn btn-light btn-block mt-3 slick-arrow" type="button" style="" aria-disabled="false" tabindex="0">Next</button>
                                    </div>
                                </div>
                            </div> <!--first-->

                            <div class="dasequend">
                                <div class="form-row mb-3">
                                    <div class="col-lg-5 col-sm-6">
                                        <label><?=__("Email",TEXT_DOMAIN);?>:</label>
                                        <input type="text" name="email" class="form-control" required placeholder="<?=__("Your email address",TEXT_DOMAIN)?>">
                                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                    <div class="col-lg-5 col-sm-6">
                                        <label><?=__("Phone Number",TEXT_DOMAIN);?>:</label>
                                        <input type="text" name="phone" class="form-control" required placeholder="<?=__("Please include area code",TEXT_DOMAIN)?>">
                                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                </div>
                                <div class="form-row mb-3">
                                    <div class="col-lg-5  col-sm-6">
                                        <label><?=__("Self paid / Insurance card",TEXT_DOMAIN);?></label>
                                        <div class="select-arrow">
                                            <select class="form-control" name="payment_type" id="payment_type">
                                                <option value=""><?=__("-Select",TEXT_DOMAIN);?></option>
                                                <option value="Self paid"><?=__("Self paid",TEXT_DOMAIN);?></option>
                                                <option value="Insurance Card"><?=__("Insurance Card",TEXT_DOMAIN);?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5  col-sm-6">
                                        <label><?=__("Appointment time",TEXT_DOMAIN);?></label>
                                        <div class="select-arrow">
                                            <select class="form-control" name="appo_time" id="appo_time">
                                                <option value=""><?=__("-Select",TEXT_DOMAIN);?></option>
                                                <option value="AM"><?=__("AM",TEXT_DOMAIN);?></option>
                                                <option value="PM"><?=__("PM",TEXT_DOMAIN);?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row mb-3 d-none upload_card_row">
                                    <div class="col-lg-10  col-sm-12">
                                        <label><?=__("Upload insurance card",TEXT_DOMAIN);?></label>
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="insurance_card">
                                                <label class="custom-file-label" for="inputGroupFile02"><?=__("Choose file");?></label>
                                            </div>
                                        </div>
                                        <small class="muted upfile-required text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                </div>
                                <div class="form-row mb-3">
                                    <div class="col-lg-10 col-sm-12">
                                        <label><?=__("Message",TEXT_DOMAIN);?>:</label>
                                        <textarea class="form-control" name="message" style="height: 70px"  required placeholder="<?=__("Please include area code",TEXT_DOMAIN)?>"></textarea>
                                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                                    </div>
                                </div>
                                <div class="form-row pt-3">
                                    <div class="col-sm-3">
                                        <button class="btn-previo btn btn-light btn-block slick-arrow" type="button">Back</button>
                                    </div>
                                    <div class="col-sm-10 col-md-7">
                                        <div
                                             class="g-recaptcha"
                                             data-sitekey="6LfmvVUUAAAAABoZxNz_1fL8S3aMl5lI0N99Si3f"
                                             data-callback="onSubmit"
                                             data-size="invisible"
                                             >
                                        </div>
                                        <button class="btn blue-btn btn-block" type="submit" id="form-submit-btn">Confirm book</button>
                                    </div>
                                </div>
                            </div><!--second-->
                        </div> <!--steps-->
                    </form>
                    <!--                   </form>-->
                </div>
            </div>
        </div>

    </div>
</section>
