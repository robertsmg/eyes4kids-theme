<?php global $us_states; ?>
<form id="app_modal_form" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="action" value="appointment_modal_form">
    <div class="container p-0 form-modal-container">
        <div class="row">
            <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="patient-fields-content">
                    <h3><?=__("Patient Information",TEXT_DOMAIN);?></h3>
                    <?php /* FORM INPUT - FIRST NAME */?>
                    <div class="modal-form-field">
                        <input id="firstname-e4k" type="text" name="name" class="form-control" required placeholder="<?=__(" Patient's Name",TEXT_DOMAIN);?>" />
                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - LAST NAME */?>
                    <div class="modal-form-field">
                        <input id="lastname-e4k" type="text" name="lastname" class="form-control" required placeholder="<?=__(" Patient's Last name",TEXT_DOMAIN);?>" />
                        <small class="muted d-none text-danger"><?=__("This field is required",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - GENDER */?>
                    <div class="modal-form-field">
                        <select id="gender-e4k" name="gender" class="form-control">
                            <option value="" disabled selected><?=__("Gender",TEXT_DOMAIN);?></option>
                            <option value="Male"><?=__("Male",TEXT_DOMAIN);?></option>
                            <option value="Female"><?=__("Female",TEXT_DOMAIN);?></option>
                        </select>
                        <small class="muted d-none text-danger"><?=__("You must select one gender",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - BIRTHDAY */?>
                    <div class="modal-form-field">
                        <div><label for="birthday"><?=__("Patient’s Date of Birth",TEXT_DOMAIN);?></label></div>
                        <?php /* BIRTHDAY - YEAR [AUTOCOMPLETED] */?>
                        <select name="" id="year-e4k" class="form-control-date">
                            <option value="" disabled selected><?=__(" Year",TEXT_DOMAIN);?></option>
                        </select>
                        <?php /* BIRTHDAY - MONTH [AUTOCOMPLETED] */?>
                        <select name="" id="month-e4k" class="form-control-date" disabled>
                            <option value="" disabled selected><?=__(" Month",TEXT_DOMAIN); ?></option>
                        </select>
                        <?php /* BIRTHDAY - DAY [AUTOCOMPLETED BY SELECTION] */?>
                        <select name="" id="day-e4k" class="form-control-date" disabled>
                            <option value="" disabled selected><?=__(" Day",TEXT_DOMAIN); ?></option>
                        </select>
                        <?php /* BIRTHDAY - REAL HIDDEN INPUT FOR POST */?>
                        <input id="datepicker-e4k" name="birthday" type="hidden" readonly placeholder="<?=__(" Date of Birth",TEXT_DOMAIN);?>" class="form-control" />
                        <small class="muted d-none text-danger"><?=__("Birth date must not be empty",TEXT_DOMAIN);?></small>
                    </div>
                </div>

                <div class="medical-fields-content">
                    <h3><?=__("Medical Information",TEXT_DOMAIN);?></h3>
                    <?php /* FORM INPUT - PAYMENT TYPE */?>
                    <div class="modal-form-field">
                        <select id="payment_type" class="form-control" name="payment_type">
                            <option value="" disabled selected><?=__("Self Pay/Insurance",TEXT_DOMAIN);?></option>
                            <option value="Self paid"><?=__("Self Pay",TEXT_DOMAIN);?></option>
                            <option value="Insurance"><?=__("Insurance",TEXT_DOMAIN);?></option>
                        </select>
                        <small class="muted d-none text-danger"><?=__("Must select one option",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - INSURANCE NAME [AUTOSHOWN BY PREVIOUS SELECTION] */ ?>
                    <div id="insurance_name_field" class="modal-form-field modal-form-field-hidden">
                        <input id="insurance_name_input" class="form-control" name="insurance_name" placeholder="<?=__(" Insurance Name",TEXT_DOMAIN);?>" value="" />
                        <small class="muted d-none text-danger"><?=__("You must enter a valid insurance name",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - PRIMARY PREFERRED APPOINTMENT DATE/TIME */ ?>
                    <div class="modal-form-field">
                        <div>
                            <label for="appointment-primary-day-e4k"><?=__("Preferred Appointment Date & Time",TEXT_DOMAIN);?></label>
                        </div>
                        <?php /* PRIMARY PREFERRED APPOINTMENT DATE/TIME - DATE */ ?>
                        <select id="appointment-primary-day-e4k" name="primary-preferred-day" class="form-control form-control-col-2">
                            <option value="" disabled selected><?=__("Preferred day",TEXT_DOMAIN);?></option>
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                            <option value="Friday">Friday</option>
                        </select>
                        <?php /* PRIMARY PREFERRED APPOINTMENT DATE/TIME - TIME */ ?>
                        <select id="appointment-primary-time-e4k" name="primary-preferred-time" class="form-control form-control-col-2">
                            <option value="" disabled selected><?=__("Preferred time",TEXT_DOMAIN); ?></option>
                            <option value="AM"><?=__("AM",TEXT_DOMAIN);?></option>
                            <option value="PM"><?=__("PM",TEXT_DOMAIN);?></option>
                        </select>
                        <small class="muted d-none text-danger"><?=__("You must choose day and time options",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - MESSAGE */ ?>
                    <div class="modal-form-field">
                        <textarea class="form-control" id="message-e4k" name="message" style="height: 70px" required placeholder="<?=__(" Message",TEXT_DOMAIN);?>"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="contact-fields-content">
                    <h3><?=__("Contact Information",TEXT_DOMAIN);?></h3>
                    <?php /* FORM INPUT - ADDRESS */ ?>
                    <div class="modal-form-field">
                        <textarea id="address-e4k" name="address" class="form-control" style="height: 70px" required placeholder="<?=__(" Address",TEXT_DOMAIN);?>"></textarea>
                        <small class="muted d-none text-danger"><?=__("You must enter more than one letter",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - STATES [AUTOGENERATED] */ ?>
                    <div class="modal-form-field">
                        <select id="state-e4k" name="state" class="form-control">
                            <option value="" disabled selected><?=__("State",TEXT_DOMAIN);?></option>
                            <?php foreach ($us_states as $key => $state) {?>
                            <option value="<?=$key?>"><?=$state?></option>
                            <?php }?>
                        </select>
                        <small class="muted d-none text-danger"><?=__("You must select one state",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - ZIP CODE */ ?>
                    <div class="modal-form-field">
                        <input id="zipcode-e4k" name="zipcode" type="number" class="form-control" placeholder="<?=__(" Zip Code",TEXT_DOMAIN);?>" />
                        <small class="muted d-none text-danger"><?=__("This field can't be empty",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - EMAIL ADDRESS */ ?>
                    <div class="modal-form-field">
                        <input id="email-e4k" name="email" type="email" class="form-control" required placeholder="<?=__(" Email",TEXT_DOMAIN);?>" />
                        <small class="muted d-none text-danger"><?=__("You must enter a valid email adress",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - PHONE NUMBER */ ?>
                    <div class="modal-form-field">
                        <input id="phone-e4k" name="phone" type="tel" class="form-control" required placeholder="<?=__(" Phone Number",TEXT_DOMAIN);?>" />
                        <small class="muted d-none text-danger"><?=__("You must enter a valid phone number",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* FORM INPUT - RECAPTCHA */ ?>
                    <div class="modal-form-field">
                        <!-- CAPTCHA FOR FINAL SERVER -->
                        <div class="g-recaptcha" data-sitekey="6LcxZnMUAAAAAO0eeRNL72SWKwKBf26UjKCu-3Wb" data-callback="onSubmit" data-size="small"></div>
                        <small class="muted d-none text-danger"><?=__("You must validate this Captcha",TEXT_DOMAIN);?></small>
                    </div>
                    <?php /* SUBMIT FORM */ ?>
                    <button id="form-submit-btn" type="submit" class="btn blue-btn btn-block">Request Appointment</button>
                    <?php /* AJAX LOADER */ ?>
                    <div id="form-status-controller" class="form-status"></div>
                </div>
            </div>
        </div>
    </div>

</form>
