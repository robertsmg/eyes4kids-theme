<section class="no-results not-found">
    <header class="page-header">
        <h1 class="page-title"><?php _e('There\'s no results', TEXT_DOMAIN); ?></h1>
    </header>
    <!-- .page-header -->

    <div class="page-content">
        <p><?php _e('Sorry, There is not results for this search. Please try again with other terms',TEXT_DOMAIN); ?></p>
        <?php get_search_form(); ?>
    </div>
    <!-- .page-content -->
</section><!-- .no-results -->
