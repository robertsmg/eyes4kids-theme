<div class="blog-side-container">
    <?php get_search_form();?>
    <?php
    $cur_cat = false;
    if(get_query_var('cat')){
        $cur_cat = get_category(get_query_var('cat'));
    } else {
        $cur_cat = (object) [
            'term_id' => 1
        ];
    }
    $categories =  get_categories(array('hide_empty' => false, 'exclude' => array(1)));
    ?>
    <h3 class='cats-heading'>
        <?php _e("Categories",TEXT_DOMAIN)?>
    </h3>
    <ul class="categories-list">
        <?php
    if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
        foreach  ($categories as $category) {
            $class = ($cur_cat->term_id == $category->term_id)? "active":"";
            echo '<li><a class ="'.$class.'" href="'.get_category_link($category->term_id).'">'.$category->name.'</a></li>';
        }
    }
        ?>
    </ul>
    <h3 class='cats-heading'>
        <?php _e("Subscribe via email",TEXT_DOMAIN)?>
    </h3>
    <form id="subscriber_form" class="search-form" method="post">
        <input type="text" class="form-control  wb " placeholder="<?=__(" Name");?>" name="subscriber_name" id="subscriber_name">
        <small class="muted d-none text-danger"><?=__("You must enter a valid name",TEXT_DOMAIN);?></small>

        <input type="email" class="form-control  wb mt1" placeholder="<?=__(" Email");?>" name="subscriber_email" id="subscriber_email">
        <small class="muted d-none text-danger"><?=__("You must enter a valid email adress",TEXT_DOMAIN);?></small>

        <?php wp_nonce_field('add_subscriber'); ?>
        <!-- Requerido -->
        <input type="hidden" name="action" value="add_subscriber"> <!-- Requerido -->

        <button type="submit" id="send_subscriber" class="btn blue-btn btn-subscriber">
            <?=__("Submit");?></button>
    </form>
</div>
