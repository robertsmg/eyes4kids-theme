<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <?php global $title ?>
    <title><?php echo $title ?></title>
</head>
<body>

<div style="color:#444; max-width: 600px; border: 1px solid #cccccc; padding: 15px; box-shadow: 0 0 2px #999999; margin: auto; font-family:Open-sans, sans-serif;">
    <h2 style="margin-bottom: 2px; margin-top: 2px;"><?php echo $title ?></h2>
    <p style="margin-top: 2px; margin-bottom: 2px">Sent: <?php echo date("Y/m/d h:i") ?></p>
    <hr style="border: solid 2px #444">
    <div style="border: solid 1px #cccccc; background-color: #eeeeee; padding: 15px; margin-top: 15px;">
        <?php
        global $contact_fields, $submit,$us_states;

        foreach ($contact_fields as $key => $field) {
            $field_value = apply_filters('mailto', $submit[$key]);
            if($key == "state"){
                $field_value = $us_states[$field_value];
            }
            if($field_value == ""){
                continue;
            }

            printf('<p style="margin: 5px 0;"><strong>%s</strong>: %s</p>', $field, $field_value);
        }
        ?>
    </div>
</div>

</body>
</html>