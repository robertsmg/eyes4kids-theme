<?php $meta_cards = get_post_meta($post->ID,"site_appointment_links",true); ?>
<section class="appointment d-flex" id="appointment">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="appointment-container col-12">

                <div class="container">
                    <div class="row row-appointment">
                        <div class="appointment-ellipse"></div>
                        <div class="appointment-text col-12 col-sm-12 col-md-8 col-lg-7">
                            <?php foreach ($meta_cards as $key => $card) {?>
                            <?=apply_filters("the_content",$card["site_link_text"])?>
                            <?php } ?>
                        </div>
                        <div class="w-100"></div>
                        <div class="appointment-item col-12 col-lg-4">
                            <?php foreach ($meta_cards as $key => $card) {?>
                            <button class="btn btn-md btn-appointment" data-toggle="modal" data-target="#exampleModal"><?php echo $card["site_link_button"]; ?></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
