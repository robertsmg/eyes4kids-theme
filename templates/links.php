<?php

if(get_current_blog_id() == "1"){
    $post = get_page_by_path("home");
}else{
    $post = get_page_by_path("inicio");
}

$meta_links = get_post_meta($post->ID,"site_section_links",true);
?>
<section class="links">
    <div class="container">
        <div class="row links-slider">
            <?php foreach ($meta_links as $link) {?>
            <div class="col-12 col-md-6 col-sm-10 col-lg-4">
                <div class="media  py-4 text-center text-md-left">
                    <img class="mr-3 ml-3" src="<?=$link["site_link_image"]?>" alt="">
                    <div class="media-body">
                        <?=$link["site_link_text"]?>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</section>
