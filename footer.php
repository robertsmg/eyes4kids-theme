<?php
$menu1 = wp_get_nav_menu_items("footer-1");
$menu2 = wp_get_nav_menu_items("footer-2");
?>



<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 text-center text-lg-left pb-3">
                <img src="<?=get_stylesheet_directory_uri()."/img/logo-footer.png"?>" alt="Eyes for kids" />
            </div>
            <div class="col-lg-3">
                <ul class="text-center text-lg-left list-unstyled mb-0">
                    <?php foreach ($menu1 as $item) {?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=$item->url?>">
                            <i class="fa fa-chevron-right"></i>
                            <?=$item->title?>
                        </a>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <div class="col-lg-3">
                <ul class="text-center text-lg-left list-unstyled mb-0">
                    <?php foreach ($menu2 as $item) {?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=$item->url?>">
                            <i class="fa fa-chevron-right"></i>
                            <?=$item->title?>
                        </a>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <div class="col-lg-3 col-info text-center text-lg-left">
                <?=__("Call us",TEXT_DOMAIN);?><br />
                <strong><a href="tel:+13056628390">+1-305-662-8390</a></strong>
                <br>
                <?=__("Send an email",TEXT_DOMAIN);?>:<br>
                <strong><a href="mailto:info@eyes4kids.com">info@eyes4kids.com</a></strong>
            </div>
        </div>
        <div class="row mt-4 d-none d-md-block ">
            <div class="col-12 text-center">
                <img class="img-fluid" src="<?=get_stylesheet_directory_uri()."/img/AAPOS-directoy.jpg"?>" alt="">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-6 text-center text-md-left">
                &copy;
                <?=date("Y")?>,
                <?=__("Pediatric Ophthalmology Consultants, Miami, Florida",TEXT_DOMAIN);?>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <a href="http://screenmediagroup.com">DEVELOPED BY SMG | DIGITAL MARKETING AGENCY</a>
            </div>
        </div>
    </div>
</footer>

</div><!-- .site-wrapper-->

<!-- Modal -->
<div class="modal fade custom-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?=__("Make an appointment",TEXT_DOMAIN);?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <?php get_template_part('templates/form-modal-body'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?&key=<?=STATIC_API_KEY?>"></script>
<?php wp_footer() ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>

</html>
