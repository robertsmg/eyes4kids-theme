<?php
global $post;
the_post();
get_header();
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>

<section class="banner-page"  style="background-image: url('<?=$image[0]?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-7 d-md-flex align-items-center ">
                <div>
                    <?php the_title("<h1>","</h1>"); ?>
                    <?php the_excerpt(); ?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php


?>
<section class="as-seen">
    <div class="container">
        <div class="row justify-content-center">
            <?php foreach (get_post_meta($post->ID,"site_about_icon_list",true) as $icon) {?>
                <div class="col-md-2 text-center d-flex align-items-center">
                    <img src="<?=$icon["site_icon_img"]?>" />
                </div>
            <?php }?>

        </div>
    </div>
</section>
<section class="about-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <?php the_content() ?>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="banner-page extra-packet-info"  style="background-image: url('<?=get_stylesheet_directory_uri()."/img/about-bot.jpg"?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-7 d-md-flex align-items-center ">
                <div>
                   <h2><?=__("Over 30 years providing personalized and cutting edge pediatric eye care",TEXT_DOMAIN);?></h2>
                    <a href="<?=site_url()?>#appointment" class="btn blue-btn px-5 mt-4   "><?=__("Make an appointment",TEXT_DOMAIN);?></a>
                </div>

            </div>
        </div>
    </div>
</section>


<?php get_footer();