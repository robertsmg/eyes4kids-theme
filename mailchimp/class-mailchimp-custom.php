<?php

class MailchimpCustom {

  /**
   * Endpoint for Mailchimp API v3
   *
   * @var string
   */
  private $endpoint = 'https://us1.api.mailchimp.com/3.0/';
  /**
   * @var string
   */
  private $apikey = 'e916e71ac8fea1a8e66a913e6882bc1d-us17';

  /**
   * @param string $apikey
   * @param array $clientOptions
   */
  public function __construct( $apikey = '' ) {
    if ( !empty( $apikey ) ) {
      $this->apikey = $apikey;
    }
    $this->detectEndpoint( $this->apikey );
  }

  /**
   * @param $apikey
   */
  public function detectEndpoint( $apikey ) {
    if ( !strstr( $apikey, '-' ) ) {
      throw new InvalidArgumentException( 'There seems to be an issue with your apikey. Please consult Mailchimp' );
    }
    list(, $dc) = explode( '-', $apikey );
    $this->endpoint = str_replace( 'us1', $dc, $this->endpoint );
  }

  /**
   * @param string $apikey
   */
  public function setApiKey( $apikey ) {
    $this->detectEndpoint( $apikey );
    $this->apikey = $apikey;
  }

  /**
   * @return string
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  public function add_member( $list, $data ) {
    $headers = array( 'Authorization' => 'apikey ' . $this->apikey );

    $endpoint = $this->getEndpoint() . 'lists/' . $list . '/members';

    $args = array(
        'method'  => 'POST',
        'body'    => json_encode( $data ),
        'headers' => $headers
    );

    $response = wp_remote_post( $endpoint, $args );
    return json_decode( $response['body'] );
  }

  public function update_member( $list, $data ) {
    $headers  = array( 'Authorization' => 'apikey ' . $this->apikey );
    $endpoint = $this->getEndpoint() . 'search-members?query=' . $data['email_address'];

    $args = array(
        'method'  => 'GET',
        'headers' => $headers
    );

    $_data = array();

    foreach ( $data as $key => $value ) {
      if ( $value ) {
        $_data[$key] = $value;
      }
    }

    $response = wp_remote_post( $endpoint, $args );
    $response = json_decode( $response['body'] );

    if ( !empty( $response->exact_matches->members[0] ) ) {
      $subscriber_hash = $response->exact_matches->members[0]->id;

      $endpoint = $this->getEndpoint() . 'lists/' . $list . '/members/' . $subscriber_hash;

      $args = array(
          'method'  => 'PUT',
          'body'    => json_encode( $_data ),
          'headers' => $headers
      );

      $response = wp_remote_post( $endpoint, $args );
      return true;
    } else {
      return false;
    }
  }

  public function get_interest_categories( $list_id, $interest_category_id = '' ) {

    $transient = get_transient( "mailchimp_interest_{$list_id}{$interest_category_id}" );

    if ( empty( $transient ) ) {
      $headers  = array( 'Authorization' => 'apikey ' . $this->apikey );
      $endpoint = $this->getEndpoint() . 'lists/' . $list_id . '/interest-categories/' . $interest_category_id;
      $args     = array(
          'method'  => 'GET',
          'headers' => $headers
      );
      $response = wp_remote_post( $endpoint, $args );
      $response = json_decode( $response['body'] );

      $transient['title'] = $response->title;

      $headers  = array( 'Authorization' => 'apikey ' . $this->apikey );
      $endpoint = $this->getEndpoint() . 'lists/' . $list_id . '/interest-categories/' . $interest_category_id . '/interests';
      $args     = array(
          'method'  => 'GET',
          'headers' => $headers
      );
      $response = wp_remote_post( $endpoint, $args );

      $response = json_decode( $response['body'] );


      $items = array();

      foreach ( $response->interests as $item ) {
        $items[$item->id] = $item->name;
      }

      $transient['items'] = $items;

      set_transient( "mailchimp_interest_{$list_id}{$interest_category_id}", $transient, DAY_IN_SECONDS );
    }

    return $transient;
  }

}
