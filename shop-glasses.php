<?php
/**
 * Template Name: Shop Glasses
 *
 */
get_header();
the_post();
global $post;
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
$posti = $post;
//var_dump(get_post_meta($posti->ID));
?>

<section class="banner-page"  style="background-image: url('<?=$image[0]?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-6 d-flex align-items-center ">
                <div>
                    <?php the_title("<h1>","</h1>"); ?>
                    <?=apply_filters("the_content",get_post_meta($posti->ID,"site_top_text_sg",true))?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php get_template_part("templates/links");?>

<section class=" our-practice-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php the_content() ?>
            </div>
        </div>
    </div>
</section>

<section class="banner-page extra-packet-info"  style="background-image: url('<?=get_stylesheet_directory_uri()."/img/1st-visit-banner.jpg"?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-7 d-md-flex align-items-center ">
                <div>
                    <?=apply_filters("the_content",get_post_meta($posti->ID,"site_bottom_text_sg",true))?>
                </div>

            </div>
        </div>
    </div>
</section>


<?php  get_footer();
