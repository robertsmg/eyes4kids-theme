var lastScrollTop = 0,
    allowSubmit = false,
    passd = true,
    owl = $('.links-slider'),
    testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i,
    locale = frontend_ajax_object.multiste_id !== '1' ? 'es-es' : 'en-us';

$(function () {

    $(document).ready(function () {
        "use strict";

        if (jQuery(window).width() < 768) {
            jQuery('.links-slider').addClass('owl-carousel');
            owl.owlCarousel({
                items: 1
            });
        }

        jQuery(window).resize(function () {
            if (jQuery(window).width() < 768) {
                jQuery('.links-slider').addClass('owl-carousel');
                owl.owlCarousel({
                    items: 1
                });
            } else {
                owl.owlCarousel('destroy');
                jQuery('.links-slider').removeClass('owl-carousel');
            }
        });
    });

    if ($("#map").length > 0) {
        google.maps.event.addDomListener(window, 'load', init);
    }

    function init() {
        //return;
        var mapOptions = {
            scrollwheel: false,
            draggable: !("ontouchend" in document),
            zoom: 16,
            center: new google.maps.LatLng({
                lat: 25.7414451,
                lng: -80.2942208
            })
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
    }
    $(".appointment .date").mask("00/00/0000", {
        placeholder: "__/__/____"
    });
    $('.appointment .phone_us').mask('(000) 000-0000');
    /* MAILCHIMP SUBSCRIPTION */
    jQuery('#subscriber_name').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    jQuery('#subscriber_email').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });
    jQuery("#subscriber_form").on("submit", function (e) {
        e.preventDefault();
        passd = true;

        if (($('#subscriber_name').val() == '') || ($('#subscriber_name').val() == null) || ($('#subscriber_name').val().length < 3)) {
            $('#subscriber_name').next("small").removeClass("d-none");
            passd = false;
        } else {
            $('#subscriber_name').next("small").addClass("d-none");
        }


        if (($('#subscriber_email').val() == '') || ($('#subscriber_email').val() == null) || ($('#subscriber_email').val().length < 3)) {
            $('#subscriber_email').next("small").removeClass("d-none");
            passd = false;
        } else {
            $('#subscriber_email').next("small").addClass("d-none");
        }

        //        if ($("#payment_type").val == "Insured") {
        //            if ($('#inputGroupFile02').prop('files').length == 0) {
        //                $(".upfile-required").removeClass("d-none");
        //                passd = false;
        //            }
        //        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: frontend_ajax_object.admin_url,
                data: jQuery('#subscriber_form').serialize(),
                beforeSend: function (formData, jqForm, options) {
                    jQuery('#send_subscriber').html('Sending');
                },
                success: function (data) {
                    jQuery('#send_subscriber').html(data);
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        }
    });

    $(document).on('click', 'a[href="#appointment"]', function (event) {

        var href = $.attr(this, 'href');
        if (href != "#appointment") {
            return;
        }
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 130
        }, 500, function () {
            window.location.hash = href;
        });
    });


    if (document.location.href.match("#appointment")) {
        $('html, body').animate({
            scrollTop: $($("#appointment")).offset().top - 130
        }, 500, function () {
            //window.location.hash = href;
        });
    }

    $('.navbar-nav a').on('click', function () {

        $('.dropdown').children('.dropdown-menu').slideUp(300);
        if ($(this).parent().hasClass("show")) {
            $(this).next('.dropdown-menu').slideUp(300);
        } else {
            $(this).next('.dropdown-menu').slideDown(300);
        }
    });

    /* CONTACT FORM */
    jQuery('#fullname-contact').on('focusout', function () {
        if ((jQuery(this).val() == '') || (jQuery(this).val() == null) || (jQuery(this).val().length < 3)) {
            jQuery(this).next('small').removeClass('d-none');
        } else {
            jQuery(this).next('small').addClass('d-none');
        }
    });

    jQuery('#phone-contact').on('focusout', function () {
        if ((jQuery(this).val() == '') || (jQuery(this).val() == null) || (jQuery(this).val().length < 3)) {
            jQuery(this).next('small').removeClass('d-none');
        } else {
            jQuery(this).next('small').addClass('d-none');
        }
    });

    jQuery('#email-contact').on('focusout', function () {
        if ((jQuery(this).val() == '') || (jQuery(this).val() == null) || (jQuery(this).val().length < 3)) {
            jQuery(this).next('small').removeClass('d-none');
        } else {
            jQuery(this).next('small').addClass('d-none');
        }
    });

    jQuery('#reason-contact').on('focusout', function () {
        if ((jQuery(this).val() == '') || (jQuery(this).val() == null)) {
            jQuery(this).next('small').removeClass('d-none');
        } else {
            jQuery(this).next('small').addClass('d-none');
        }
    });

    jQuery('#message-contact').on('focusout', function () {
        if ((jQuery(this).val() == '') || (jQuery(this).val() == null) || (jQuery(this).val().length < 3)) {
            jQuery(this).next('small').removeClass('d-none');
        } else {
            jQuery(this).next('small').addClass('d-none');
        }
    });

    /* FORM SUBMIT CONTROL */

    jQuery("#contact-form").on("submit", function (e) {
        e.preventDefault();
        passd = true;
        if (allowSubmit === true) {
            $(".g-recaptcha").next("small").addClass("d-none");
        } else {
            $(".g-recaptcha").next("small").removeClass("d-none");
        }

        $("#contact-form .form-control").each(function () {
            var $el = $(this);

            if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
                $el.next("small").removeClass("d-none");
                passd = false;
            } else {
                $el.next("small").addClass("d-none");
            }

        });

        $("#contact-form .form-control [required]").each(function () {
            var $el = $(this);
            if ($el.val() == '') {
                $el.next("small").removeClass("d-none");
                passd = false;
            } else {
                $el.next("small").addClass("d-none");
            }
        });

        if (testEmail.test($('input[type=email]').val())) {
            $('input[type=email]').next("small").addClass("d-none");
        } else {
            $('input[type=email]').next("small").removeClass("d-none");
        }

        //        if ($("#payment_type").val == "Insured") {
        //            if ($('#inputGroupFile02').prop('files').length == 0) {
        //                $(".upfile-required").removeClass("d-none");
        //                passd = false;
        //            }
        //        }

        if (passd == true) {

            jQuery.ajax({
                type: 'POST',
                url: frontend_ajax_object.admin_url,
                data: jQuery('#contact-form').serialize(),
                beforeSubmit: function (formData, jqForm, options) {
                    jQuery('#form-status-controller').html('<div class="loader-1"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>');
                },
                success: function (data) {
                    console.log(data);
                    jQuery('#form-status-controller').html(data);
                }
            });
            /* OLD FORM */
            /*
            jQuery('#app_modal_form').ajaxForm({
                url: frontend_ajax_object.admin_url,
                data: {},
                dataType: 'json',
                beforeSubmit: function (formData, jqForm, options) {
                    jQuery('.form-status').html('<div class="loader-1"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>');
                },
                success: function (response, statusText, xhr, $form) {
                    jQuery('.form-status').html('<div class="loader-1"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>');
                    // code that's executed when the request is processed successfully
                    if (response.success) {
                        alert(response);
                    } else {
                        jQuery('.form-status').removeAttr('disabled').text('Fail');
                    }
                }
            });
            */
            /* OLD FORM */
        }
    });
});

onSubmit = function () {
    allowSubmit = true;
};

$(window).on("load", function () {
    if ($("#form-submit-btn").length > 0) {
        grecaptcha.reset();
    }
});
