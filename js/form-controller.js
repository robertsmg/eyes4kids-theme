var allowSubmit = false,
    testEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/,
    passd = true,
    monthName = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayQuantity = 0,
    validMonthLength = ['', 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

$(function () {

    $(document).ready(function () {
        "use strict";

        prefill_select_vars('year-e4k', '1930', '120');
        prefill_select_vars('month-e4k', '1', '12');
        prefill_select_vars('day-e4k', '1', '31');
    });


    /* PRE FILL - DATE VARIABLES */
    function prefill_select_vars(select_id, init, limit) {
        select_data = document.getElementById(select_id);

        if (select_id === 'month-e4k') {
            for (i = 0; i < parseInt(limit); i++) {
                var y = parseInt(init) + i;
                select_data.add(new Option(monthName[y], y), null);
            }
        } else {
            for (i = 0; i < parseInt(limit); i++) {
                var y = parseInt(init) + i;
                select_data.add(new Option(y, y), null);
            }
        }
    }

    /* DATE OPTIONS - LEAP YEAR */
    function leapYear(year) {
        var result;
        year = parseInt(document.getElementById("year-e4k").value);
        return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    }

    /* DATE OPTIONS - FUNCTIONS */

    $('#year-e4k').on('change', function () {
        $('#month-e4k').removeAttr('disabled');

        if (($('#month-e4k').val() !== null) && ($('#day-e4k').val() !== null)) {
            var d = $('#year-e4k').val() + '/' + $('#month-e4k').val() + '/' + $('#day-e4k').val();
            $('#datepicker-e4k').val(d);
        }
    });

    $('#month-e4k').on('change', function () {
        var valueSelected = this.value;
        var isleapYear = leapYear($('#year-e4k').value);

        $('#day-e4k').removeAttr('disabled');

        $('#day-e4k')
            .find('option')
            .remove()
            .end()
            .append('<option value=" " disabled selected>Day</option>');

        if (isleapYear === true) {
            dayQuantity = validMonthLength[valueSelected] + 1;
        } else {
            dayQuantity = validMonthLength[valueSelected];
        }

        prefill_select_vars('day-e4k', 1, dayQuantity);

        if (($('#month-e4k').val() !== null) && ($('#day-e4k').val() !== null)) {
            var d = $('#year-e4k').val() + '/' + $('#month-e4k').val() + '/' + $('#day-e4k').val();
            $('#datepicker-e4k').val(d);
        }
    });

    $('#day-e4k').on('change', function () {
        if (($('#month-e4k').val() !== null) && ($('#day-e4k').val() !== null)) {
            var d = $('#year-e4k').val() + '/' + $('#month-e4k').val() + '/' + $('#day-e4k').val();
            $('#datepicker-e4k').val(d);
        }
    });

    /* PAYMENT OPTIONS */
    //    $("#payment_type").on("change", function () {
    //        if ($(this).val() === "Insurance Card") {
    //            $("#inputGroupFile02").attr("required", "required");
    //            $(".upload_card_row").removeClass("d-none");
    //        } else {
    //            $("#inputGroupFile02").removeAttr("required");
    //            $(".upload_card_row").addClass("d-none");
    //        }
    //    });

    /* VALIDATIONS */
    $('#firstname-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#lastname-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#gender-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#payment_type').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#insurance_name_input').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#appointment-primary-day-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#appointment-primary-time-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#address-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#state-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#zipcode-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#email-e4k').on('focusout', function () {

        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    $('#phone-e4k').on('focusout', function () {
        if (($(this).val() == '') || ($(this).val() == null) || ($(this).val().length < 3)) {
            $(this).next('small').removeClass('d-none');
        } else {
            $(this).next('small').addClass('d-none');
        }
    });

    /* FORM SUBMIT CONTROL */

    jQuery("#app_modal_form").on("submit", function (e) {
        e.preventDefault();
        passd = true;
        if (allowSubmit === true) {
            $(".g-recaptcha").next("small").addClass("d-none");
        } else {
            $(".g-recaptcha").next("small").removeClass("d-none");
        }

        $("#app_modal_form .form-control").each(function () {
            var $el = $(this);
            if ($el.attr('id') !== 'message-e4k') {
                if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
                    $el.next("small").removeClass("d-none");
                    passd = false;
                } else {
                    $el.next("small").addClass("d-none");
                }
            }
        });

        $("#app_modal_form .form-control [required]").each(function () {
            var $el = $(this);
            if ($el.val() == '') {
                $el.next("small").removeClass("d-none");
                passd = false;
            } else {
                $el.next("small").addClass("d-none");
            }
        });

        if (($('#email-e4k').val() == '') || ($('#email-e4k').val() == null) || ($('#email-e4k').val().length < 3)) {
            $('#email-e4k').next("small").removeClass("d-none");
        } else {
            $('#email-e4k').next("small").addClass("d-none");
        }

        //        if ($("#payment_type").val == "Insured") {
        //            if ($('#inputGroupFile02').prop('files').length == 0) {
        //                $(".upfile-required").removeClass("d-none");
        //                passd = false;
        //            }
        //        }

        if (passd == true) {

            jQuery.ajax({
                type: 'POST',
                url: frontend_ajax_object.admin_url,
                data: jQuery('#app_modal_form').serialize(),
                beforeSubmit: function (formData, jqForm, options) {
                    jQuery('#form-status-controller').html('<div class="loader-1"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>');
                },
                success: function (data) {
                    window.location = data;
                }
            });
            /* OLD FORM */
            /*
            jQuery('#app_modal_form').ajaxForm({
                url: frontend_ajax_object.admin_url,
                data: {},
                dataType: 'json',
                beforeSubmit: function (formData, jqForm, options) {
                    jQuery('.form-status').html('<div class="loader-1"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>');
                },
                success: function (response, statusText, xhr, $form) {
                    jQuery('.form-status').html('<div class="loader-1"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>');
                    // code that's executed when the request is processed successfully
                    if (response.success) {
                        alert(response);
                    } else {
                        jQuery('.form-status').removeAttr('disabled').text('Fail');
                    }
                }
            });
            */
            /* OLD FORM */
        }
    });

    jQuery('#payment_type').on('change', function () {
        "use strict";
        var optionSelected = jQuery("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected === 'Insurance') {
            jQuery('#insurance_name_field').removeClass('modal-form-field-hidden');
            jQuery('#insurance_name_input').val('');
        } else {
            jQuery('#insurance_name_field').addClass('modal-form-field-hidden');
            jQuery('#insurance_name_input').val('Self Pay');
            jQuery('#insurance_name_input').attr('value', 'Self Pay');
        }
    });
});

onSubmit = function () {
    allowSubmit = true;
};

$(window).on("load", function () {
    if ($("#form-submit-btn").length > 0) {
        grecaptcha.reset();
    }
});

$('.modal').on('shown.bs.modal', function (e) {
    $('html').addClass('freezePage');
    $('body').addClass('freezePage');
});
$('.modal').on('hidden.bs.modal', function (e) {
    $('html').removeClass('freezePage');
    $('body').removeClass('freezePage');
});
