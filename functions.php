<?php
define("THEME_PREFIX","e4k_");
define("TEXT_DOMAIN","e4k");
define("THEME_VERSION","0.0.1");


define("STATIC_API_KEY","AIzaSyCa69Q7MRLBufjmJA2ReDjpAzpcH8sMt5M");

include_once("includes/reset.php");
include_once("includes/types.php");
include_once("includes/meta.php");

function getLastInstagram()
{
    //$home_page = get_post(5);

    //$access_token = get_post_meta($home_page->ID,"cra_instagram_token",true);
    //var_dump($home_page);
    //$access_token = "428111644.1677ed0.1d4eaac5f1144b339717d17d6966f8e3";
    $access_token = "6732855575.1677ed0.2e36cadd8f81490abbaa8b79868aae1c";
    $url = 'https://api.instagram.com/v1/users/self/media/recent?access_token=' . $access_token . '&count=12';
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 2
    ));

    $result = curl_exec($ch);
    curl_close($ch);
    $decoded_results = json_decode($result, true);
    return $decoded_results;
}


function appointment_modal_form_callback() {
    session_start();
    global $contact_fields;
    $contact_fields = array(
        'name' => 'Client\'s Name',
        'lastname' => 'Client\'s lastname',
        'phone' => 'Phone number',
        'gender' => 'Gender',
        'birthday' => 'Date of birth',
        'address' => 'Address',
        'state' => 'State',
        'zipcode' => 'Zipcode',
        'email' => 'Email',
        'primary-preferred-day' => 'Preferred Appointment Day',
        'primary-preferred-time' => 'Preferred Appointment Time',
        'payment_type' => 'Payment type',
        'insurance_name' => 'Name of Insurance',
        'message' => 'Message',
    );

    //    $captcha = $_POST["g-recaptcha-response"];
    //    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcxZnMUAAAAAEizWfI29vgsVyezhGxBl0hQJXzQ&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
    if ($_POST["g-recaptcha-response"]) {
        $post_data = http_build_query(
            array(
                'secret' => '6LcxZnMUAAAAAEizWfI29vgsVyezhGxBl0hQJXzQ',
                'response' => $_POST['g-recaptcha-response'],
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ), '', '&');
        $opts = array('http' =>
                      array(
                          'method'  => 'POST',
                          'header'  => 'Content-type: application/x-www-form-urlencoded',
                          'content' => $post_data
                      )
                     );
        $context  = stream_context_create($opts);
        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
        $result = json_decode($response);
    }
    if($result->success == true) {

        global $title;
        $title = "Eyes 4 Kids Appointment Email";
        global $submit;


        $submit = wp_parse_args($_POST);


        $have_file = false;
        if($submit["payment_type"]=="Insurance Card"){
            $have_file = true;
            $filename = $_FILES['insurance_card']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('pdf');
            if( ! in_array( $ext, $allowed ) ) {
                $message_cv = 'Oops! Invalid file extension, only pdf files are allowed';
                $_SESSION["message_cv"] = $message_cv;
                wp_redirect( wp_get_referer());
                die();
            }

            if ( ! function_exists( 'wp_handle_upload' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
            }
            $uploadedfile = $_FILES['insurance_card'];
            $upload_overrides = array( 'test_form' => false );
            $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
            $binary_content = file_get_contents($movefile["file"]);
        }


        ob_start();
        get_template_part('templates/contact_email');
        $content = ob_get_clean();

        require_once ABSPATH . WPINC . '/class-phpmailer.php';
        $mail = new PHPMailer();
        $email = get_option("admin_email");

        if($have_file){
            $mail->AddStringAttachment($binary_content, "insurance_card.pdf",'base64','application/pdf');
        }

        $mail->AddAddress($email);
        $mail->AddAddress( 'nefrain@gmail.com' );
        //$mail->AddAddress("rochoa@screen.com.ve");
        $mail->From = 'noreply@' . $_SERVER['SERVER_NAME'];
        $mail->FromName = get_option("blogname")." - ".get_option("blogdescription");
        $mail->Subject = $title;
        $mail->Body = $content;
        $mail->IsHTML();
        $mail->CharSet = 'utf-8';

        $result1 = $mail->Send();
        after_subscription_mailchimp($submit["email"],$submit["name"],$submit["lastname"],$submit["birthday"]);
        $message_cv= __("Thank you! Your submission has been received. You will be contact soon to confirm your appointment.",TEXT_DOMAIN);;
        if(session_status()!=2){
            session_start();
        }
        $_SESSION["message_cv"] = $message_cv;
        echo home_url('/thanks');
    } else {
        echo 'error';
    }

    die();

}
add_action( 'wp_ajax_nopriv_appointment_modal_form', 'appointment_modal_form_callback' );
add_action( 'wp_ajax_appointment_modal_form', 'appointment_modal_form_callback' );



function send_contact_form_mail() {

    session_start();
    global $contact_fields;
    $contact_fields = array(
        'fullname' => 'Client\'s Name',
        'phone' => 'Phone number',
        'email' => 'Email',
        'reason' => 'Addressed to',
        'booked_before' => 'Has booked before',
        'message' => 'Message',
    );
    if ($_POST["g-recaptcha-response"]) {
        $post_data = http_build_query(
            array(
                'secret' => '6LcxZnMUAAAAAEizWfI29vgsVyezhGxBl0hQJXzQ',
                'response' => $_POST['g-recaptcha-response'],
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ), '', '&');
        $opts = array('http' =>
                      array(
                          'method'  => 'POST',
                          'header'  => 'Content-type: application/x-www-form-urlencoded',
                          'content' => $post_data
                      )
                     );
        $context  = stream_context_create($opts);
        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
        $result = json_decode($response);
    }
    if($result->success == true) {
        global $title;
        $title = "Eyes 4 Kids Contact Email";
        global $submit;


        $submit = wp_parse_args($_POST);


        ob_start();
        get_template_part('templates/appointment_email');
        $content = ob_get_clean();

        require_once ABSPATH . WPINC . '/class-phpmailer.php';
        $mail = new PHPMailer();
        $email = get_option("admin_email");

        //$mail->AddAddress("rochoa@screen.com.ve");
        switch($submit["reason"]){
            case "Dr. Warman MD":
                $email = "rwarman@eyes4kids.com";
                break;
            case "Contact Lenses":
                $email = "cls@eyes4kids.com";
                break;
            case "Optical Shop Surgeries":
                $email = "Rachel@eyes4kids.com";
                break;
            case "Surgeries":
                $email = "pperez@eyes4kids.com";
                break;
            default:
                $email = "appointments@eyes4kids.com";
        }

        $mail->AddAddress($email);

        $mail->From = 'noreply@' . $_SERVER['SERVER_NAME'];
        $mail->FromName = get_option("blogname")." - ".get_option("blogdescription");
        $mail->Subject = $title;
        $mail->Body = $content;
        $mail->IsHTML();
        $mail->CharSet = 'utf-8';

        $result1 = $mail->Send();
        $message_cv= __("Thank you! Your submission has been received. You will be contact soon to confirm your appointment.",TEXT_DOMAIN);


        if(session_status()!=2){
            session_start();
        }
        $_SESSION["message_cv"] = $message_cv;
        echo $message_cv;
    }
    else
    {
        $message_cv= __("Error! Your submission has not been received. Please try again.",TEXT_DOMAIN);
        echo $message_cv;
    }

    die();
}
add_action( 'wp_ajax_nopriv_contact_form', 'send_contact_form_mail' );
add_action( 'wp_ajax_contact_form', 'send_contact_form_mail' );




function after_subscription_mailchimp($email,$name,$lastname,$birthday){
    include_once "mailchimp/class-mailchimp-custom.php";

    $list = "e297c46cb0";

    if(!empty($birthday)){
        $birthday = date("m/d",strtotime($birthday));
    }
    $data      = array(
        'email_address' => $email,
        "status"        => "subscribed",
        "merge_fields"  => array(
            'FNAME'    =>$name,
            'LNAME'    =>$lastname,
            "BIRTHDAY" => $birthday
        ),
    );

    $mailchimp = new MailchimpCustom();
    $response  = $mailchimp->add_member( $list, $data );
    if ( 'Member Exists' == $response->title ) {
        $mailchimp->update_member( $list, $data );
    }
}

function add_subscriber_form_callback () {
    $nonce = $_POST['_wpnonce'];
    if (wp_verify_nonce( $nonce, 'add_subscriber')) {
        include_once "mailchimp/class-mailchimp-custom.php";

        $list = "ebbadebade";

        $email = htmlentities($_POST['subscriber_email']);
        $nombre_largo = $_POST['subscriber_name'];

        $nombre = explode(' ', $nombre_largo);

        $data      = array(
            'email_address' => $email,
            "status"        => "subscribed",
            "merge_fields"  => array(
                'FNAME' => $nombre[0],
                'LNAME'    => $nombre[1]
            ),
        );

        $mailchimp = new MailchimpCustom();
        $response  = $mailchimp->add_member( $list, $data );
        if ( 'Member Exists' == $response->title ) {
            $mailchimp->update_member( $list, $data );
        }
        echo 'Success';
    } else {
        echo 'Fail';
    }
    die();

}

add_action('wp_ajax_nopriv_add_subscriber', 'add_subscriber_form_callback');
add_action('wp_ajax_add_subscriber', 'add_subscriber_form_callback');

function custom_pagination() {
    global $wp_query;
    $big = 999999999;
    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?page=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_next' => false,
        'type' => 'array',
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
    ));
    if (is_array($pages)) {
        $current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ($pages as $i => $page) {
            if ($current_page == 1 && $i == 0) {
                echo "<li class='active'>$page</li>";
            } else {
                if ($current_page != 1 && $current_page == $i) {
                    echo "<li class='active'>$page</li>";
                } else {
                    echo "<li>$page</li>";
                }
            }
        }
        echo '</ul>';
    }
}

if (!is_admin()) {
    function wpb_search_filter($query) {
        if ($query->is_search) {
            $query->set('post_type', 'post');
        }
        return $query;
    }
    add_filter('pre_get_posts','wpb_search_filter');
}

global $us_states;
$us_states =

    array(
    'AL' => 'Alabama',
    'AK' => 'Alaska',
    'AZ' => 'Arizona',
    'AR' => 'Arkansas',
    'CA' => 'California',
    'CO' => 'Colorado',
    'CT' => 'Connecticut',
    'DE' => 'Delaware',
    'DC' => 'District Of Columbia',
    'FL' => 'Florida',
    'GA' => 'Georgia',
    'HI' => 'Hawaii',
    'ID' => 'Idaho',
    'IL' => 'Illinois',
    'IN' => 'Indiana',
    'IA' => 'Iowa',
    'KS' => 'Kansas',
    'KY' => 'Kentucky',
    'LA' => 'Louisiana',
    'ME' => 'Maine',
    'MD' => 'Maryland',
    'MA' => 'Massachusetts',
    'MI' => 'Michigan',
    'MN' => 'Minnesota',
    'MS' => 'Mississippi',
    'MO' => 'Missouri',
    'MT' => 'Montana',
    'NE' => 'Nebraska',
    'NV' => 'Nevada',
    'NH' => 'New Hampshire',
    'NJ' => 'New Jersey',
    'NM' => 'New Mexico',
    'NY' => 'New York',
    'NC' => 'North Carolina',
    'ND' => 'North Dakota',
    'OH' => 'Ohio',
    'OK' => 'Oklahoma',
    'OR' => 'Oregon',
    'PA' => 'Pennsylvania',
    'RI' => 'Rhode Island',
    'SC' => 'South Carolina',
    'SD' => 'South Dakota',
    'TN' => 'Tennessee',
    'TX' => 'Texas',
    'UT' => 'Utah',
    'VT' => 'Vermont',
    'VA' => 'Virginia',
    'WA' => 'Washington',
    'WV' => 'West Virginia',
    'WI' => 'Wisconsin',
    'WY' => 'Wyoming',
);
