<?php
get_header();

$terms = get_terms( 'category', array(
    'hide_empty' => false,
    'slug' => ['faqs','eyes-facts','eyes-diseases']

) );
//var_dump($terms);
$curr_cat = get_query_var('cat');

?>

<section class="categories-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 mb-2">
                <div class="col-resources">
                    <a href="<?php echo get_category_link( $terms[0]->term_id ); ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title <?=($curr_cat == $terms[0]->term_id ? "current":"")?>"><?=$terms[0]->name?></h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4 mb-2">
                <div class="col-eyes-facts">
                    <a href="<?php echo get_category_link( $terms[1]->term_id ); ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title <?=($curr_cat == $terms[1]->term_id ? "current":"")?>"><?=$terms[1]->name?></h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4  mb-2">
                <div class="col-eyes-diseases">
                    <a href="<?php echo get_category_link( $terms[2]->term_id ); ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title <?=($curr_cat == $terms[2]->term_id ? "current":"")?>"><?=$terms[2]->name?></h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="posts-columns mb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-col">

                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post();?>
                    <div class="row mb-5">

                            <div class="col-sm-12">
                                <a href="<?php the_permalink() ?>" class="title">
                                    <?php the_title() ?>
                                </a>

                                <div class="post-meta">
                                    <?php
                                    $u_time = get_the_time('U');
                                    $u_modified_time = get_the_modified_time('U');
                                    if ($u_modified_time >= $u_time + 86400) {?>
                                        <?php echo __("Updated")." ".get_the_modified_time('F jS, Y');
                                    }else{
                                        the_time( 'F d, Y');
                                    }
                                    ?>
                                    <?=__("by");?> <?php the_author_posts_link();?>
                                </div>
                                <?php if (has_post_thumbnail()) { ?>
                                <div class="img-holder">
                                    <a href="<?php the_permalink() ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                                    </a>
                                </div>
                                <?php } ?>
                                <div class="excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>

                    </div>
                    <?php endwhile;?>
                    <?php
                        else :
                            get_template_part('templates/content', 'none');
                        endif;
                    ?>


            </div>
            <div class="col-lg-4">
                <?php get_template_part("templates/blog-side"); ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-center">
                <div class="navigation"><p><?php posts_nav_link('&#8734;','Previous posts','Next posts'); ?></p></div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();
