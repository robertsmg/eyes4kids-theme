<?php
get_header();
the_post();
global $post;
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
?>

<section class="banner-page"  style="background-image: url('<?=$image[0]?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-5 d-md-flex align-items-center ">
                <div>
                    <?php the_title("<h1>","</h1>"); ?>
                    <?php the_excerpt(); ?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php get_template_part("templates/links");?>

<section class=" our-practice-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php the_content() ?>
            </div>
        </div>
    </div>
</section>
<section class="banner-page extra-packet-info"  style="background-image: url('<?=get_stylesheet_directory_uri()."/img/eyesurgerybanner.jpg"?>')">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-7 d-md-flex align-items-center ">
                <div>
                    <h2><?=__("Eye surgery for kids?",TEXT_DOMAIN);?></h2>
                    <p><?=__("Facts and myths",TEXT_DOMAIN);?></p>
                    <a href="<?=site_url()?>/blog/2018/03/12/eye-surgery/" class="btn blue-btn px-5 mt-4   "><?=__("WHAT TO EXPECT?",TEXT_DOMAIN);?></a>
                </div>

            </div>
        </div>
    </div>
</section>



<?php  get_footer();
