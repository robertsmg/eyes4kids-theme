<?php get_header(); ?>
<?php the_post();
$terms = get_terms( 'category', array(
    'hide_empty' => false,
    'slug' => ['faqs','eyes-facts','eyes-diseases']

) );
//var_dump($terms);
$curr_cat = get_query_var('cat');

?>

<section class="categories-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 mb-2">
                <div class="col-resources">
                    <a href="<?php echo get_category_link( $terms[0]->term_id ); ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title <?=($curr_cat == $terms[0]->term_id ? " current":"")?>">
                                <?=$terms[0]->name?>
                            </h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4 mb-2">
                <div class="col-eyes-facts">
                    <a href="<?php echo get_category_link( $terms[1]->term_id ); ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title <?=($curr_cat == $terms[1]->term_id ? " current":"")?>">
                                <?=$terms[1]->name?>
                            </h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4  mb-2">
                <div class="col-eyes-diseases">
                    <a href="<?php echo get_category_link( $terms[2]->term_id ); ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title <?=($curr_cat == $terms[2]->term_id ? " current":"")?>">
                                <?=$terms[2]->name?>
                            </h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="posts-columns mb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-col">
                <?php the_content(); ?>
            </div>
            <div class="col-lg-4">
                <?php get_template_part("templates/blog-side"); ?>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
